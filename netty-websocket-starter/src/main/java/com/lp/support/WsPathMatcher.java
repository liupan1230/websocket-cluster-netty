package com.lp.support;

import io.netty.channel.Channel;
import io.netty.handler.codec.http.QueryStringDecoder;


/**
 * @author lp
 */
public interface WsPathMatcher {

    /**
     * xxx
     *
     * @return
     */
    String getPattern();

    /**
     * xxx
     *
     * @param decoder
     * @param channel
     * @return
     */
    boolean matchAndExtract(QueryStringDecoder decoder, Channel channel);
}
